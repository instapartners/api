FROM credimi/swagger-ui:2.1.4

MAINTAINER Instapartners <tech@instapartners.it>

RUN npm config set strict-ssl false && npm install -g yamljs && npm install -g showdown && npm install -g markdown-styles

RUN mkdir -p dist/static

COPY instapartners_api.yaml .

RUN yaml2json instapartners_api.yaml > instapartners_api.json

RUN cp instapartners_api.json dist/static/

COPY errors.md .

COPY instapartners-layout ./instapartners-layout

RUN generate-md --layout ./instapartners-layout --input ./errors.md --output ./dist/static

COPY index.html dist/

COPY swagger-ui.js dist/

COPY gulpfile.js .

COPY ftp.md .

RUN generate-md --layout /instapartners-layout --input ./ftp.md --output ./dist/static

ADD start.sh .

EXPOSE 8080

CMD ./start.sh
