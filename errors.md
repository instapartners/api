# Error types of Instapartners API

Instapartners API uses standard http codes to return errors and a rich json object to provide more actionable information for
API consumers. The rich error representation has the following structure:

```json
{
  "code": "errorCode",
  "message": "some error message",
  "developerMessage" : "a richer and more informative message for the developer",
  "moreInfo"  : "go to website xyx.com to find out more informations about the error"
}
```

Notice that:
- The *moreInfo* is optional
- The *message* will be localized in Italian and in the future more localized messages will be supported. The api will
interpret correctly the Http *Accept-Language* header and return the localized message, if available

----------------
Note: for simplicity and consistency, an error with the same structure is always returned even for non-ambiguous
status codes such as 404 NOT FOUND


# Error codes structure

The error codes are six digits numbers to be interpreted like the following:

- The first three digits correspond to the Http Status Code

- The fourth digit identifies a category of errors for that status code

- The last two digits identify the specific error

The guide will be ordered by:

- Http status code
- Then error category
- Then error code
    

 
# Error types of Instapartners API

Instapartners API uses standard http codes to return errors and a rich json object to provide more actionable information for
API consumers. The rich error representation has the following structure:

```json
{
  "code": "errorCode",
  "message": "some error message",
  "developerMessage" : "a richer and more informative message for the developer",
  "moreInfo"  : "go to website xyx.com to find out more informations about the error"
}
```

Notice that:
- The *moreInfo* is optional
- The *message* will be localized in Italian and in the future more localized messages will be supported. The api will
interpret correctly the Http *Accept-Language* header and return the localized message, if available

----------------
Note: for simplicity and consistency, an error with the same structure is always returned even for non-ambiguous
status codes such as 404 NOT FOUND


# Error codes structure

The error codes are six digits numbers to be interpreted like the following:

- The first three digits correspond to the Http Status Code

- The fourth digit identifies a category of errors for that status code

- The last two digits identify the specific error

The guide will be ordered by:

- Http status code
- Then error category
- Then error code
    

 
## 400 Bad Request : The request contains bad syntax or cannot be fulfilled.

### 400 - 0 : Invalid Input

4000000 : The user has been trying to signup with an invalid error code 

4000001 : A verification has failed because a code is not valid 

4000099 : Multiple invalid inputs were found in the request 

4000002 : A user who does not exist has tried to log in 

4000003 : A user has tried to login providing invalid credentials 
      
### 400 - 1 : Notification Error

4000100 : The server has been trying to send a notification with invalid data 

4000101 : The server has been trying to send a notification with invalid notification type 

4000102 : The server has been trying to send an invite but there was no invite id 

4000103 : The server has been trying to send a phone message to a user who had no phone in database 
      
### 400 - 2 : Constraint Violation

4000200 : A client application has been trying to create a company with a VATCODE already existing in the database 
      
    
## 401 Unauthorized : Authentication is possible but has failed or not yet been provided.

### 401 - 0 : Invalid Authentication

4010000 : A request has been sent with an invalid token to the server 

4010001 : A client has tried to authenticate with a non supported authentication mode 
      
    
## 404 Not Found : The requested resource could not be found but may be available again in the future.

### 404 - 0 : Not Found

4040000 : The requested user invite could not be found 

4040001 : The requested company invite could not be found 

4040002 : A user has tried an invite code which could not be found 
      
    
## 409 Conflict : The request could not be processed because of conflict in the request, such as an edit conflict.

### 409 - 0 : Conflict

4090000 : The user could not be created 
      
    
## 500 Internal Server Error : There was an internal server error.

### 500 - 0 : Unknown

5000001 : An unhandled exception has occurred when trying to deliver an email. Please contact the support 

5000002 : An unhandled exception has occurred when trying to deliver an email. Please contact the support 

5000000 : An unhandled exception has occurred. Please contact the support 
      
    
