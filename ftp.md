# Credimi FTP/SFTP Integration

&nbsp;

Credimi offers file-transfer integration for reverse factoring. The file-transfer integration use plain comma-separated
values (CSV) encoded with UTF-8 as a file format. This documentation is structured in three parts:

- A first part named *General concepts* which describes the high-level features and discuss many important concerns, such as error handling,
folder structures, etc.
- The second part named *File formats* which provides the details about the structure of files used in the integration
- The third part provides an example of interaction and folders.

## Table of contents

1. General concepts
    1. Flows of information
    2. Naming convention
    3. Folder structure, audit trail and updates
        1. Audit trail and updates
    4. Service level agreement
2. File formats
    1. Suppliers
        1. Upstream suppliers
            1. Creating and updating suppliers
            2. Blocking suppliers
        2. Downstream suppliers
            1. Updates on suppliers
            2. Suppliers errors
    2. Invoices
        1. Upstream invoices
            1. Creating and updating invoices
            2. Blocking invoices
        2. Downstream invoices 
            1. Updates on invoices
            2. Errors on invoices


## 1. General concepts

This first part of the documentation will introduce you to general concepts which will help you during the integration: flows of information, error
management, updates and service level agreement.

### 1.1 Flows of information

The integration is composed of two flows of information: suppliers and invoices. These two flows generates totally four streams:

1. The reverse factoring partner uploads on a FTP or SFTP  a file containing a list of suppliers to invite on the platform.
2. The reverse factoring partner uploads on a FTP or SFTP a file containing a list of invoices to upload on the platform
3. Credimi uploads on a FTP or SFTP a file containing the advances that Credimi has performed on the invoices
4. Credimi uploads on a FTP or SFTP information about errors

To ensure separation of concerns, different folders are used:
- *upstream/suppliers*
- *upstream/invoices*
- *downstream/suppliers*
- *downstream/invoices*

where **upstream** should be interpreted from reverse factoring partner to Credimi and **downstream**  from Credimi to the partner.

### 1.2 Naming convention

For each flow, file names should be prefixed with the name of the flow followed by an underscore. These are example of valid names:
- invoice_abcd.csv for a file in the invoices flow
- suppliers_1234.csv for a file in the suppliers files


To prevent partial processing of file which are getting written, please write first a temporary file which does NOT match a valid name and then rename it.
Please consider identifying the filename by date and time by placing this information after the underscore, for example naming your files like so:
- invoices_210620161845.csv
- invoices_210620161945.csv
- invoices_210620162045.csv

### 1.3 Folder structure, audit trail and updates.

Each of the upstream flows will use subfolders:

- post   : for new or updated entries
- delete : for deleted entries

The invoice downstream flows will use the following two folders:
- . : for updates (so for example, updates on invoices are placed on downstream/invoices/invoices_1234.csv)
- errors : for errors (so for example, errors for invoices are placed on downstream/invoices/errors/invoices_1234.csv)

The suppliers downstream flows will use a single folder, errors: for example a valid filename is downstream/suppliers/errors/suppliers_12312.invoices

#### 1.3.1 Audit trail and updates

In order to guarantee full audit trail and traceability, FTP integration will be configured so that Credimi and the reverse partner will
not be permissioned to delete or overwrite files in the FTP (renaming would be allowed). A separate mechanism will be provided for handling updates:

- Each of the entities (invoices, suppliers) will have a **unique identifiers** defined in the file formats part of the documentation.
- When a new file is written on the FTP and it contains a row corresponding to a pre-existing entity, Credimi will handle that data as an update

### 1.4 Service Level Agreement

Although file-transfer is a batch process, Credimi Platform processes the data uploaded by partners in **near real-time** on a
first-arrived first-served basis. Advance data is written by Credimi to the advances folder *at worst* every half an hour. In order to
 avoid payment conflicts, Credimi will agree with each reverse partner **a cut-off days-to-expiry** beyond which a supplier could not
 require advancing an invoice anymore.

## 2. File formats

This section details the different file formats for the different flows. The following rules applies to all files:

 1. The file is encoded in UTF-8
 2. Header should not be included
 3. The semicolon ```;``` is used as a separator
 4. Decimal values use the dot ```.``` as a decimal separator

### 2.1  Suppliers

#### 2.1.1 Upstream suppliers

##### 2.1.1.1 Creating and updating suppliers

This file describes a list of suppliers which are authorized to be invited on Credimi platform by the reverse factoring partner. When
providing general information about these suppliers, the partner will provide for each of them an *External Company Id* which will be sent back on the
downstream flows. From the point of view of the reverse partner, the *External Company Id* is effectively the *unique identifier*.

Each row in the suppliers file has the following format:

| Field Index | Field Description | Format | Mandatory |  Details | Example(s)
| ---: |--------------------:| ---:|----|---------:|---------------:|
| 1 | Company Name | N/A | Yes | The complete name of the invited company | Credimi S.P.A. |
| 2 | Vat Code | Valid Vat  | Yes | The vat code of the invited company | 09171640965 |
| 3 | External Company Id | N/A | Yes | A unique identifier that the partner uses to identify the company in their own systems | 1234 |
| 4 | Full Name of Individual to Contact | N/A | Yes | The full name of the individual to invite on the platform | Sabino Costanza |
| 5 | Email of Individual to Contact | Valid Email Address | Yes| Email of the individual to invite on the platform| sabino.costanza@credimi.com |
| 6 | Phone Number | Valid Phone Number | Yes | Phone Number of the individual to contact or of the company |  333 333 333 33|
| 7 | Supplier PEC | Valid Email Address | No | Certified Email of the company | credimi@pec.it |

This is valid example of a row of the file
```
Credimi S.P.A.;09171640965;1234;Sabino Costanza;sabino.costanza@credimi.com;333333333;credimi@pec.it
```

##### 2.1.1.2 Blocking suppliers

To block a previously invited supplier the reverse factoring
partner needs to place a file in the subfolder delete where each row has the following structure:

| Field Index | Field Description | Format | Mandatory | Details | Example(s)
| ---: |--------------------:| ---:|---:|---------:|---------------:|
| 1 | External Company Id | N/A | Yes | A unique identifier that the partner uses to identify the company in their own systems | 1234 |

Notice that this will trigger setting all the invoices towards the partner to get into BLOCKED state (see invoice states below)

#### 2.1.2 Downstream suppliers

##### 2.1.2.1 Updates on suppliers

There are no updates on suppliers

##### 2.1.2.2 Suppliers errors

The error file is structured in four columns:

| Field Index | Field Description | Format | Mandatory | Details | Example(s)
| ---: |--------------------:| ---:|---:|---------:|---------------:|
| 1 | External Company Id | N/A | Yes | A unique identifier that the partner uses to identify the company in their own systems | 1234 |
| 2 | Error Code | N/A | Yes | An Error Code that uniquely identify the error type | 40093 |
| 3 | Error Message | N/A | Yes | A high-level error message | Impossible to delete suppliers 123. Supplier does not exist! |
| 4 | Detailed Error Message | N/A | Yes | A more detailed error message | You have tried to delete a supplier which does not exist on Credimi platform.|


### 2.2 Invoices

#### 2.2.1 Upstream invoices

##### 2.2.1.1 Creating and updating invoices

The this file describes a list of invoices which the reverse factoring partner has accepted and that Credimi might advance. Credimi
supports both single-installment and multiple-installment invoices: if all your invoices are single-installment invoices, you can simply set the
installment number to a fixed value..

From a partner point of a view, the installment is uniquely identified by *External Company Id - External Invoice Number - Invoice Acceptance Date -
Installment ID*. If all your invoices are single-installment invoices you can simply use a fixed value for all your invoices.

| Field Index | Field Description | Format | Mandatory | Details | Example(s)
| ---: |--------------------:| ---:|---:|---------:|---------------:|
| 1 | Invoice Number  | N/A | Yes | The number of the invoice as supplied by the invoicer | 012312321 |
| 2 | External Company Id | N/A | Yes | The External Company Id that allows to uniquely identify the invoicer company in the invoicee systems | 1234 |
| 3 | External Invoice Number | N/A | Yes | The Invoice number that allows to uniquely identify the invoice in the invoicee systems | 932 |
| 4 | Invoicee Vat Code | N/A | Yes | The Vat Code of the invoicee company ( among the vat code of companies belonging to the reverse partner group) | VatCode1 |
| 5 | Invoicee Name | N/A | Yes | The name of the invoicee company ( among the vat code of companies belonging to the reverse partner group) | ß |
| 6 | InstallmentNumber | Integer | Yes | The number of the installment inside the invoice | 1 |
| 7 | Invoice Date  | dd/MM/yyyy  | Yes | The date at which the invoice has been "created" by the invoicer | 21/06/2015 |
| 8 | Installment Expiry Date | dd/MM/yyyy | Yes | The expiry date of the invoice |  21/09/2015 |
| 9 | Invoice Acceptance Date | dd/MM/yyyy |  Yes | The date at which the invoice has been accepted by the reverse partner |  25/06/2015 |
| 10 | Installment Amount (VAT Included) | Decimal | Yes | The amount, VAT included, of the installment in euros | 3475.30 |

This is valid example of a row of the file
```
N123321;1234;932;VatCode1;1;20/03/2016;20/06/2016;25/03/2016;10563.30;
```

##### 2.2.1.2 Deleting invoices

To delete a previously uploaded invoice (note: this operation will fail if the invoice has already been advanced on the platform) the reverse factoring
partner needs to place a file in the subfolder delete where each row has the following structure:

| Field Index | Field Description | Format | Mandatory | Details | Example(s)
| ---: |--------------------:| ---:|---:|---------:|---------------:|
| 1 | External Company Id | N/A | Yes | A unique identifier that the partner uses to identify the company in their own systems | 1234 |
| 2 | External Invoice Number | N/A | Yes | The Invoice number that allows to uniquely identify the invoice in the invoicee systems | 932 |
| 3 | Invoicee Vat Code | N/A | Yes | The Vat Code of the invoicee company ( among the vat code of companies belonging to the reverse partner group) | VatCode1 |
| 4 | Invoice acceptance Date  | dd/MM/yyyy | Yes | The date at which the invoice has been accepted by the reverse partner | 20/06/2016 |


#### 2.2.2 Downstream invoices

##### 2.2.2.1 Updates on invoices

This file dumps the state of all the installments that the reverse partner has uploaded in the last year to day: if for example today is the first
of september 2016, every file will contain all the invoices from the first of september 2015 to the first of
september 2016.

The invoices can have the following states:

- States in which the installment is waiting to be priced and cannot be advanced
- States in which the installment can be advanced
- States in which the installment is advanced
- Blocked states

When the invoice has been uploaded but it hasn't been priced yet, it is in status
- WAITING-FOR-QUOTE
Invoices in this status cannot be advanced.

The invoice can be advanced only if it is in one of the states:

- READY-FOR-ADVANCE
the partner has not been reached yet, you from a partner point of view this is technically identical to READY-FOR-ADVANCE)

The invoice is advanced if it is in one of the states:
- FULLY-ADVANCED
- PARTIALLY-ADVANCED

Note that the PARTIALLY-ADVANCED status might not apply to you if your agreement with Credimi excludes this possibility.

The invoice is out of the portal and cannot be advanced anymore if it is in one the states:

- CUTOFF-EXCEEDED if the invoice is too close to expiration and can't be advanced anymore
- BLOCKED if the partner has cancelled the invitation to the supplier
- REJECTED if Credimi has not proposed the supplier to advance that installment
- REFUSED if the supplier has deleted the invoice from the portal

Each row inside the file will have the following structure:

| Field Index | Field Description | Format | Mandatory | Details | Example(s)
| ---: |--------------------:| ---:|---:|---------:|---------------:|
| 1 | External Company Id | N/A | Yes | The External Company Id that allows to uniquely identify the invoicer company in the invoicee systems | 1234 |
| 2 | External Invoice Number | N/A | Yes | The Invoice number that allows to uniquely identify the installment in the invoicee systems | 932 |
| 3 | Invoice acceptance Date | dd/MM/yyyy | Yes | The date at which the invoice has been accepted by the reverse partner | 20/06/2016
| 4 | Invoicee Vat Code | N/A | Yes | The Vat Code of the invoicee company ( among the vat code of companies belonging to the reverse partner group) | VatCode1 |
| 5 | InstallmentNumber | Integer | Yes | The number of the installment inside the invoice | 1 |
| 6 | Advance date | dd/MM/yyyy | No | The date at which the invoice has been advanced | 21/06/2016 |
| 7 | Advanced Amount (VAT Included) | Decimal | No | The amount of the invoice that has been advanced, if the update is an advance | 3475.30 |
| 8 | Installment state | The state of the installment | Yes |  Enumeration (see above) |
| 9 | Agreed Payment Date | dd/MM/yyyy | No | The date in which the installment will be paid back to Credimi | 20/06/2016 |
| 10 | Invoice Number  | N/A | Yes | The number of the invoice as supplied by the invoicer | 012312321 |

##### 2.2.2.2 Errors on invoices

The error file is structured in 8 columns:

| Field Index | Field Description | Format | Mandatory | Details | Example(s)
| ---: |--------------------:| ---:|---:|---------:|---------------:|
| 1 | External Company Id | N/A | Yes | A unique identifier that the partner uses to identify the company in their own systems | 1234 |
| 2 | External Invoice Number | N/A | Yes | The Invoice number that allows to uniquely identify the invoice in the invoicee systems | 932 |
| 3 | Invoicee Vat Code | N/A | Yes | The Vat Code of the invoicee company ( among the vat code of companies belonging to the reverse partner group) | VatCode1 |
| 4 | Invoice acceptance Date  | dd/MM/yyyy | Yes | The date at which the invoice has been accepted by the reverse partner | 20/06/2016 |
| 5 | Error Code | N/A | Yes | An Error Code that uniquely identify the error type | 40093 |
| 6 | Error Message | N/A | Yes | A high-level error message | Impossible to delete suppliers 123. Supplier does not exist! |
| 7 | Detailed Error Message | N/A | Yes | A more detailed error message | You have tried to delete a supplier which does not exist on Credimi platform.|
| 8 | Invoice Number  | N/A | Yes | The number of the invoice as supplied by the invoicer | 012312321 |




### Example set of folders

Credimi will provide you two credentials: one for a test ftp and one for a production ftp.

On each of the ftp you will have access to a single folder, for example mycompany.
If this root folder mycompany, the following structure will be used:

- /mycompany/upstream/invoices/post/invoices_abcd.csv
- /mycompany/upstream/suppliers/post/suppliers_213321.csv
- /mycompany/upstream/invoices/delete/invoices_efgh.csv
- /mycompany/upstream/suppliers/delete/suppliers_1111.csv
- /mycompany/downstream/suppliers/errors/suppliers_1234.csv
- /mycompany/downstream/invoices/invoices_111.csv
- /mycompany/downstream/invoices/errors/invoices_211.csv
